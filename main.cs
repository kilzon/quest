namespace TextAdventure
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(); // Создается экземпляр игры (Game) и вызывается метод Start() для начала игры.
            game.Start();
        }
    }


    class Game
    {
        private Player player; // Приватное поле, представляющее игрока.
        private bool gameOver; // Приватное поле, указывающее на окончание игры.

        public Game()
        {
            player = new Player(); // Создается новый игрок при создании экземпляра игры.
            gameOver = false; // Устанавливается значение gameOver в false, чтобы игра начиналась.
        }

        public void Start()
        {
            Console.WriteLine("Добро пожаловать в текстовый квест!");
            Console.WriteLine("Введите свое имя: ");
            player.Name = Console.ReadLine();

            Console.WriteLine($"Привет, {player.Name}! Начинаем игру.");

            while (!gameOver) // Цикл, который продолжается, пока игра не закончится.
            {
                Console.WriteLine("Выберите действие:");
                Console.WriteLine("1. Пойти налево");
                Console.WriteLine("2. Пойти направо");
                Console.WriteLine("3. Выйти из игры");

                string input = Console.ReadLine(); // Игрок выбирает действие.

                switch (input)
                {
                    case "1":
                        GoLeft(); // Вызывается метод для действия "пойти налево".
                        break;
                    case "2":
                        GoRight(); // Вызывается метод для действия "пойти направо".
                        break;
                    case "3":
                        gameOver = true; // Устанавливается значение gameOver в true, чтобы завершить игру.
                        Console.WriteLine("Вы вышли из игры.");
                        break;
                    default:
                        Console.WriteLine("Некорректный ввод. Попробуйте еще раз.");
                        break;
                }

                Console.WriteLine();
            }
        }

        private void GoLeft()
        {
            Console.WriteLine("Вы направились налево.");

            // Логика для пути налево
            Console.WriteLine("Вы видите дверь. Что вы хотите сделать?");
            Console.WriteLine("1. Открыть дверь");
            Console.WriteLine("2. Искать другой путь");

            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    // Вероятность встречи с врагом
                    if (new Random().Next(1, 11) > 5) // Генерируется случайное число от 1 до 10, и если оно больше 5, игрок вступает в бой с врагом.
                    {
                        FightEnemy(); // Вызывается метод для сражения с врагом.
                    }
                    else
                    {
                        Console.WriteLine("Вы открыли дверь и нашли сокровища! Поздравляем, вы выиграли!"); // Если число меньше или равно 5, игрок находит сокровища и выигрывает игру.
                        gameOver = true; // Устанавливается значение gameOver в true, чтобы завершить игру.
                    }
                    break;
                case "2":
                    Console.WriteLine("Вы решаете искать другой путь и поворачиваете обратно.");
                    break;
                default:
                    Console.WriteLine("Некорректный ввод. Попробуйте еще раз.");
                    break;
            }
        }

        private void GoRight()
        {
            Console.WriteLine("Вы направились направо.");

            // Логика для пути направо
            Console.WriteLine("Вы видите препятствие. Что вы хотите сделать?");
            Console.WriteLine("1. Преодолеть препятствие");
            Console.WriteLine("2. Искать другой путь");

            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    Obstacle obstacle = new Obstacle("Стена"); // Создается новое препятствие с названием "Стена".
                    obstacle.HandleObstacle(player); // Вызывается метод для обработки препятствия.
                    break;
                case "2":
                    Console.WriteLine("Вы решаете искать другой путь и поворачиваете обратно.");
                    break;
                default:
                    Console.WriteLine("Некорректный ввод. Попробуйте еще раз.");
                    break;
            }
        }

        private void FightEnemy()
        {
            Console.WriteLine("Вы встретили врага! Что вы хотите сделать?");
            Console.WriteLine("1. Атаковать врага");
            Console.WriteLine("2. Попытаться убежать");

            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    Enemy enemy = new Enemy("Злой монстр", 20, 10); // Создается новый враг
                    Console.WriteLine($"Вы сражаетесь с {enemy.Name}!");

                    // Вероятность победы над врагом
                    if (new Random().Next(1, 11) > 4)
                    {
                        Console.WriteLine($"Вы победили {enemy.Name}! Продолжайте свой путь.");
                    }
                    else
                    {
                        Console.WriteLine($"Вы не смогли победить {enemy.Name}. Вас атакуют и вы проигрываете!");
                        gameOver = true;
                    }
                    break;
                case "2":
                    Console.WriteLine("Вы пытаетесь убежать от врага, но не смогли сбежать.Вас атакуют и вы проигрываете!");
                    gameOver = true;
                    break;
                default:
                    Console.WriteLine("Некорректный ввод. Попробуйте еще раз.");
                    break;
            }
        }
    }

    class Player
    {
        public string Name { get; set; } // Свойство, представляющее имя игрока.
        public int Health { get; set; } // Свойство, представляющее здоровье игрока.

        public Player()
        {
            Health = 100; // Устанавливается начальное значение здоровья игрока.
        }
    }

    class Enemy
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public int Damage { get; set; }

        public Enemy(string name, int health, int damage)
        {
            Name = name; // Устанавливается имя, здоровье и урон врага при его создании.
            Health = health;
            Damage = damage;
        }
    }

    class Obstacle
    {

        public string Name { get; set; } // Свойство, представляющее название препятствия.

        public Obstacle(string name)
        {
            Name = name; // Устанавливается название препятствия при его создании.
        }

        public void HandleObstacle(Player player)
        {
            Console.WriteLine($"Вы столкнулись с препятствием: {Name}");

            // Вероятность преодоления препятствия
            if (new Random().Next(1, 11) > 5)
            {
                Console.WriteLine($"Вы успешно преодолели {Name} и продолжили путь.");
            }
            else
            {
                Console.WriteLine($"Вы не смогли преодолеть {Name}. Ваше здоровье снижено.");
                player.Health -= 10;
                Console.WriteLine($"Ваше текущее здоровье: {player.Health}");
                if (player.Health <= 0)
                {
                    Console.WriteLine("Ваше здоровье опустилось до нуля. Вы проиграли!");
                    Environment.Exit(0);
                }
            }
        }
    }
}
